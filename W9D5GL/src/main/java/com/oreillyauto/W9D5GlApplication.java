package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W9D5GlApplication {

	public static void main(String[] args) {
		SpringApplication.run(W9D5GlApplication.class, args);
	}

}
